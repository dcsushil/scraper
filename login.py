from selenium import webdriver
import time
import random


#to delay actions between events
def delay ():
    time.sleep(random.randint(2,3))

USERNAME = 'yourusername'
PASSWORD ='yourpassword'
URL="https://raidforums.com/member.php?action=login"
driver = webdriver.Chrome()
driver.get(URL)
delay()


user_input = driver.find_element_by_name('username')
user_input.send_keys(USERNAME)
time.sleep(1)

password_input = driver.find_element_by_name('password')
password_input.send_keys(PASSWORD)
time.sleep(1)

login_button=driver.find_element_by_name('submit')
login_button.click()

time.sleep(5)

#resolving captcha
#switching to iframe since the required captcha element is located in iframe
frame = driver.find_element_by_xpath('//iframe[contains(@src, "hcaptcha")]')
driver.switch_to.frame(frame)
checkbox = driver.find_element_by_xpath('//div[@id="checkbox" and @class="checkbox"]')
checkbox.click()
delay()

#switching back to default content
driver.switch_to.default_content()


submitcaptcha = driver.find_element_by_xpath('//input[@name="submit" and @class="button"]')
submitcaptcha.click()
