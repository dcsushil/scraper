from selenium import webdriver
import mysql.connector
import time
import random


#Database Connection
try:
    Myconnection = mysql.connector.connect(user='root',password='password',host='localhost',db='ScrapeDB')
    cursor = Myconnection.cursor()

except:
       print("No connection")

#creating table for storing the scraped data
cursor.execute("CREATE TABLE scrapedata(SN INT AUTO_INCREMENT PRIMARY KEY, Post_title varchar(100),Date_time varchar(50),User varchar(20));")


#to delay actions between events
def delay():
    time.sleep(random.randint(2,3))

#Scraping from specfic section called Forum Cracking Tutorials
URL="https://raidforums.com/Forum-Cracking-Tutorials"
driver = webdriver.Chrome()
driver.get(URL)
delay()

titles=driver.find_elements_by_xpath('//span[@class="forum-display__thread-subject"]')
date_time=driver.find_elements_by_xpath('//span[@class="forum-display__thread-date"]')
user=driver.find_elements_by_xpath('//span[@class="author smalltext"]')
delay()
cursor = Myconnection.cursor()

for i in range(len(titles)):
    cursor.execute("""INSERT INTO scrapedata(Post_title,Date_time,User) VALUES (%s, %s, %s)""",[titles[i].text, date_time[i].text, user[i].text])
    Myconnection.commit()

driver.close()
